package camt.se234.lab10.service;

import camt.se234.lab10.entity.Grade;
import org.springframework.stereotype.Service;

@Service
public class GradeServiceImpl implements GradeService {
    @Override
    public String getGrade(double score) {
        if (score >= 79) {
            return "A";
        }
        else if (score >= 75){
            return "B";
        }else if (score >= 59.1){
            return "C";
        }else if (score >= 34) {
            return "D";
        } else
            return "F";

    }
}
